Lexical analysis and comparison of the Brown Corpus and Hyperion by John Keats. 

### Contributors
- Jakob Coles
- Kamand Shayegan
- vader.csv generated with [this library](https://github.com/bagustris/text-vad)

### Libraries used
- fuzzywuzzy
- math
- matplotlib
- nltk
- numpy
- pandas
- re
- requests
- scipy
- sklearn

### Instructions
Upload notebook to Google Colab and run the cells in order. If running offline, note that there are two files (stored in this repo) which are stored online and remotely accessed.
